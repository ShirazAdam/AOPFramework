﻿using System.Diagnostics;
using Contracts.Service;

namespace Services
{
    public class Logging : ILogging
    {
        public void Log(string message)
        {
            Debug.WriteLine(message);
        }
    }
}
