﻿using System;
using Contracts.Service;

namespace Services
{
    public class Interceptor : IInterceptor
    {
        public void WriteTheOutput(string textToWrite)
        {
            System.IO.File.WriteAllText("D:\\testaop.txt", textToWrite);
        }

        public void ThrowMeAnException()
        {
            throw new Exception("test");
        }
    }
}
