﻿using System.Web.Mvc;
using Contracts.Service;

namespace AOP.Controllers
{
    public class HomeController : Controller
    {
        private readonly IInterceptor _interceptor;

        public HomeController(IInterceptor interceptor)
        {
            _interceptor = interceptor;
        }

        public ActionResult Index()
        {
            _interceptor.WriteTheOutput("home");
            _interceptor.ThrowMeAnException();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}