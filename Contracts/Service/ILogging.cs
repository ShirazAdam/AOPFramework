﻿namespace Contracts.Service
{
    public interface ILogging
    {
        void Log(string message);
    }
}
