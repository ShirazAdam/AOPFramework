﻿namespace Contracts.Service
{
    public interface IInterceptor
    {
        void WriteTheOutput(string textToWrite);

        void ThrowMeAnException();
    }
}
